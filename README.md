# AngularJS and Ruby on Rails Sample App for MMRD #
## 1.0 ##

This is a sample app using MySQL, Ruby on Rails for API and AngularJS for UI.


## Requirements ##

* RVM - https://rvm.io/rvm/install
* Ruby - ```rvm install ruby-2.4.0```
* RubyGems - ```rvm rubygems current```
* Ruby on Rails - ```gem install rails```
* MySQL - https://dev.mysql.com/downloads/installer/
* Node.js - https://nodejs.org/en/download/

## MySQL Setup ##

Create user and grant permissions -

```
#!sql

CREATE DATABASE mmrd_angular_rails;

CREATE USER 'mmrd'@'localhost' IDENTIFIED BY 'mmrd@123';

GRANT ALL ON mmrd_angular_rails.* TO 'mmrd'@'localhost';

SHOW GRANTS FOR 'mmrd'@'localhost';
```

## How to run? ##

**Test API -**

```
#!bash

bundle install

rails test
```

**Run API -**

```
#!bash

bundle install

rails db:migrate

rake db:seed

rails s
```  


**Build UI -**
  
  * Install node dependencies - ```npm install```
  * Install bower dependencies - ```bower install```
  
**Test UI -**

```
#!bash

gulp test

```
* If you get gulp not found error then run following command - ```npm install --global gulp-cli```

**Run UI -**
 
```
#!bash

gulp

```